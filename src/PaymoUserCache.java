import java.util.*;

/**
 * Created by pd on 11/5/16.
 */
public class PaymoUserCache {

    private Map<String, LinkedHashSet<String>> graphAdjList = new HashMap();

    /**
     * Verify whether Source and Target Users are separated by N-degrees
     * That is whether there is a path from Source to Target user that is less than or equal to N levels
     *
     * This does a bi-directional search and finds if there is a mutual overlap
     *
     * @param sourceUser
     * @param targetUser
     * @param levelN
     * @return
     */
    public int isConnectedWithinLevelNBiDirectional(String sourceUser, String targetUser, int levelN) {
        List<String> srcQ = new ArrayList();
        List<String> dstQ = new ArrayList();
        Set<String> exploredSrc =  new HashSet();
        Set<String> exploredDst =  new HashSet();

        srcQ.add(sourceUser);
        dstQ.add(targetUser);
        exploredSrc.add(sourceUser);
        exploredDst.add(targetUser);

        // set level to start at 1 and look till N
        int currentLevel = 1;

        while(!srcQ.isEmpty() && !dstQ.isEmpty()) {
            // search from source user to target user
            if (isConnetedBiDirectionalHelper(srcQ, exploredSrc, exploredDst, currentLevel, levelN))
                return currentLevel;
            currentLevel++;
            // search from target user to source user
            if (isConnetedBiDirectionalHelper(dstQ, exploredDst, exploredSrc, currentLevel, levelN))
                return currentLevel;
            currentLevel++;
        }
        return levelN + 1;
    }


    /**
     *
     *
     *
     * @param queue
     * @param selfExplored
     * @param otherExplored
     * @param currentLevel
     * @param levelN
     * @return
     */
    private boolean isConnetedBiDirectionalHelper(List<String> queue,
                                                  Set<String> selfExplored,
                                                  Set<String> otherExplored,
                                                  int currentLevel,
                                                  int levelN) {
        List<String> tempQueue = new ArrayList();
        // explore a new level
        for (int i = 0; i < queue.size(); i++) {
            // explore each User of current level
            String nextUser = queue.get(i);
            // add unexplored adjacent Users to our queue until levelN is reached
            List<String> adjList = getAdjacentNodes(nextUser);
            for (int j = 0; j < adjList.size(); j++) {
                String newAdjUser = adjList.get(j);
                // check if adjacent User is our target
                if (otherExplored.contains(newAdjUser)) {
                    // DONE. we found our target within levelN.
                    return true;
                } else if (currentLevel < levelN && !selfExplored.contains(newAdjUser)) {
                    // add to be explored only when not explored/visited earlier
                    tempQueue.add(newAdjUser);
                    // mark User explored
                    selfExplored.add(newAdjUser);
                }
            }
        }
        // we now have all the unexplored Users for the next level
        // reassign the queue to explore a new level and increment current level
        queue.clear();
        queue.addAll(tempQueue);
        return false;
    }

    /**
     *
     *
     *
     * @param user1
     * @param user2
     */
    public void addConnection(String user1, String user2) {
        addEdge(user1, user2);
        addEdge(user2, user1);
    }

    void addEdge(String node1, String node2) {
        LinkedHashSet<String> adjacent = graphAdjList.get(node1);
        if (adjacent == null) {
            adjacent = new LinkedHashSet();
            graphAdjList.put(node1, adjacent);
        }
        adjacent.add(node2);
    }

    LinkedList<String> getAdjacentNodes(String user) {
        LinkedHashSet<String> adjList = graphAdjList.get(user);
        if (adjList == null) {
            return new LinkedList();
        }
        return new LinkedList<String>(adjList);
    }
}
