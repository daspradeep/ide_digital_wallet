import java.util.List;

/**
 * Created by pd on 11/10/16.
 *
 * Not using JUNIT here
 *
 * Hand built a few unit tests
 */
public class UnitTest {

    public static void main(String[] args) {
        testGetAdjacentNodes();

        testAddEdge();

        testAddEdge2();

        testAddConnection();
    }

    public static void testAddConnection() {
        String u1 = "1";
        String u2 = "2";

        PaymoUserCache paymoUserCache = new PaymoUserCache();

        paymoUserCache.addConnection(u1, u2);

        List<String> adjList = paymoUserCache.getAdjacentNodes(u2);
        if (adjList == null || adjList.size() != 1 || !adjList.get(0).equals(u1)) {
            System.err.println("testAddConnection Failed");
        }
    }

    public static void testGetAdjacentNodes() {
        String u1 = "1";
        String u2 = "2";

        PaymoUserCache paymoUserCache = new PaymoUserCache();

        paymoUserCache.addEdge(u1, u2);

        List<String> adjList = paymoUserCache.getAdjacentNodes(u1);
        if (adjList == null || adjList.size() != 1 || !adjList.get(0).equals(u2)) {
            System.err.println("testGetAdjacentNodes Failed");
        }
    }

    public static void testGetAdjacentNodes2() {
        String u1 = "1";
        String u2 = "2";
        String u3 = "3";
        String u4 = "4";

        PaymoUserCache paymoUserCache = new PaymoUserCache();

        paymoUserCache.addEdge(u1, u2);
        paymoUserCache.addEdge(u2, u1);
        paymoUserCache.addEdge(u2, u3);
        paymoUserCache.addEdge(u2, u4);


        List<String> adjList = paymoUserCache.getAdjacentNodes(u2);
        if (adjList == null || adjList.size() != 3) {
            System.err.println("testGetAdjacentNodes2 Failed");
        }
    }

    public static void testAddEdge() {
        String u1 = "1";
        String u2 = "2";

        PaymoUserCache paymoUserCache = new PaymoUserCache();

        paymoUserCache.addEdge(u1, u2);
        List<String> adjList = paymoUserCache.getAdjacentNodes(u1);
        if (adjList == null || adjList.size() != 1 || !adjList.get(0).equals(u2)) {
            System.err.println("testAddEdge Failed");
        }
    }

    public static void testAddEdge2() {
        String u1 = "1";
        String u2 = "2";
        String u3 = "3";

        PaymoUserCache paymoUserCache = new PaymoUserCache();

        paymoUserCache.addEdge(u1, u2);
        paymoUserCache.addEdge(u1, u3);

        List<String> adjList = paymoUserCache.getAdjacentNodes(u1);
        if (adjList == null || adjList.size() != 2 || !(adjList.get(0).equals(u2) || adjList.get(0).equals(u3))) {
            System.err.println("testAddEdge2 Failed");
        }
    }

}
