import java.util.regex.Pattern;

/**
 * Created by pd on 11/9/16.
 *
 * A simple POJO to represent the Paymo Payment data
 *
 */
public class PaymoPayment {

    public static final Pattern SPLIT_BY_COMMA = Pattern.compile(",");

    private String payerId;
    private String payeeId;

    /**
     * Validate input data and build a PaymoPayment object from data with following format
     *
     * time, id1, id2, amount, message
     * 2016-11-02 09:49:29, 52575, 1120, 25.32, Food
     *
     * For now, we only validate the Payer and Payee id to be integers
     *
     * @param line
     * @return
     */
    public static PaymoPayment build(String line) {
        PaymoPayment paymoPayment = null;
        String[] values = SPLIT_BY_COMMA.split(line);
        if (values.length > 4) {
            String id1 = values[1].trim();
            String id2 = values[2].trim();
            try {
                Integer.valueOf(id1);
                Integer.valueOf(id2);
                paymoPayment = new PaymoPayment();
                paymoPayment.payerId = id1;
                paymoPayment.payeeId = id2;
            } catch (NumberFormatException e) {
                // Log bad input
            }
        } else {
            // Log bad input
        }
        return paymoPayment;
    }

    public String getPayerId() {
        return payerId;
    }

    public String getPayeeId() {
        return payeeId;
    }
}
