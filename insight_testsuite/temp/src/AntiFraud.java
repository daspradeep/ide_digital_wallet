import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.regex.Pattern;

public class AntiFraud {

    public static final boolean TEST = false;

    public static final String TRUSTED = "trusted\n";
    public static final String UNVERIFIED = "unverified\n";

    public static final Pattern SPLIT_BY_COMMA = Pattern.compile(",");

    private static String batchInputFile = "./paymo_input/batch_payment.csv";
    private static String streamInputFile = "./paymo_input/stream_payment.csv";

    private static String output1 = "./paymo_output/output1.txt";
    private static String output2 = "./paymo_output/output2.txt";
    private static String output3 = "./paymo_output/output3.txt";

    /**
     * Main entry point that sets up the input and output files
     * and then invokes batch and stream processing
     *
     * @param args
     */
    public static void main(String[] args) {

        if (args.length > 0) {
            batchInputFile = args[0];
        }
        if (args.length > 1) {
            streamInputFile = args[1];
        }
        if (TEST) {
            batchInputFile = "./insight_testsuite/tests/test-4-paymo-trans/paymo_input/batch_payment.txt";
            streamInputFile = "./insight_testsuite/tests/test-4-paymo-trans/paymo_input/stream_payment.txt";;
        }

        if (args.length > 4) {
            output1 = args[2];
            output2 = args[3];
            output3 = args[4];
        }

        // initialize the cache object that stores the Users connection graph in memory
        PaymoUserCache paymoUserCache = new PaymoUserCache();

        // load historical data to build the existing user payment network
        batchProcess(paymoUserCache);

        // process stream data to indicate fraudulent payment requests from untrusted users
        streamProcess(paymoUserCache);

    }

    /**
     * Reads the batch input file that contains the past data to build the initial state of the entire user network
     *
     * @param paymoUserCache
     */
    private static void batchProcess(PaymoUserCache paymoUserCache) {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(batchInputFile), StandardCharsets.UTF_8)) {
            for (String line; (line = br.readLine()) != null;) {
                PaymoPayment paymoPayment = PaymoPayment.build(line);
                if (paymoPayment != null) {
                    paymoUserCache.addConnection(paymoPayment.getPayerId(), paymoPayment.getPayeeId());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * Reads the stream input file and implements feature1, feature2 and feature3
     * Writes processing oputput to corresponding output files
     *
     * @param paymoUserCache
     */
    private static void streamProcess(PaymoUserCache paymoUserCache) {
        List<Byte> results = new ArrayList();
        try (BufferedReader br = Files.newBufferedReader(Paths.get(streamInputFile), StandardCharsets.UTF_8)) {
            for (String line; (line = br.readLine()) != null;) {
                PaymoPayment paymoPayment = PaymoPayment.build(line);
                if (paymoPayment != null) {
                    int level = paymoUserCache.isConnectedWithinLevelNBiDirectional(paymoPayment.getPayerId(),
                                                                                    paymoPayment.getPayeeId(), 4);
                    // collect result
                    results.add((byte)level);
                    // add payer - payee connection, irrespective of the result
                    paymoUserCache.addConnection(paymoPayment.getPayerId(), paymoPayment.getPayeeId());
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        writeOutput(results);
    }

    private static void writeOutput(List<Byte> results) {
        File file1 = new File(output1);
        File file2 = new File(output2);
        File file3 = new File(output3);
        try {
            file1.delete();
            file1.createNewFile();
            file2.delete();
            file2.createNewFile();
            file3.delete();
            file3.createNewFile();

            FileWriter writer1 = new FileWriter(file1);
            FileWriter writer2 = new FileWriter(file2);
            FileWriter writer3 = new FileWriter(file3);

            for (Byte result: results) {
                writer1.write(result <= 1 ? TRUSTED : UNVERIFIED);
                writer2.write(result <= 2 ? TRUSTED : UNVERIFIED);
                writer3.write(result <= 4 ? TRUSTED : UNVERIFIED);
            }

            writer1.flush();
            writer1.close();
            writer2.flush();
            writer2.close();
            writer3.flush();
            writer3.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }

}
